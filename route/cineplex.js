const { Router } = require("express");
const { Cineplex, Movie } = require("./../models");
const {
  getAllCineplex,
  getAllCinemaByCineplex,
  getLichChieuHeThongRap,
  getLichChieuPhim,
  createCineplex,
  updateCineplex,
  removeCineplex,
} = require("../controller/cineplex.controller");
const {
  checkMaHeThongRapQuery,
} = require("../middleware/validation/checkMaHeThongRap");
const {
  checkExistMaPhimQuery,
} = require("../middleware/validation/checkExistMaPhimQuery.middleware");
const {
  authenticate,
  authorize,
} = require("../middleware/auth/verify-token.middleware");
const cineplexRouter = Router();
cineplexRouter.get("/", getAllCineplex);
cineplexRouter.get(
  "/layThongTinCumRapTheoHeThong",
  checkMaHeThongRapQuery(Cineplex),
  getAllCinemaByCineplex
);
cineplexRouter.get(
  "/layLichChieuTheoPhim",
  checkExistMaPhimQuery(Movie),
  getLichChieuPhim
);
cineplexRouter.put(
  "/",
  authenticate,
  authorize(["QuanTri"]),

  updateCineplex
);
cineplexRouter.delete(
  "/",
  authenticate,
  authorize(["QuanTri"]),
  checkMaHeThongRapQuery(Cineplex),
  removeCineplex
);
cineplexRouter.get("/layLichChieuBangHeThongRap", getLichChieuHeThongRap);
cineplexRouter.post("/", authenticate, authorize(["QuanTri"]), createCineplex);
module.exports = {
  cineplexRouter,
};
