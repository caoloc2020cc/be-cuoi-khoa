const { sequelize } = require("./../../models");
const checkNgayChieuGioChieu = (model) => async (req, res, next) => {
  const { ngayChieuGioChieu, maRap } = req.body;
  if (!ngayChieuGioChieu || ngayChieuGioChieu == true) {
    res
      .status(400)
      .send("Ban hay nhap ngay chieu gio chieu theo YYYY/MM/DD hh:mm:ss");
  } else if (
    ngayChieuGioChieu &&
    !ngayChieuGioChieu.match(
      /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4} ([0-1][0-9]|[2][0-3]):([0-5][0-9]):([0-5][0-9])$/
    )
  ) {
    res
      .status(500)
      .send("Ngay chieu khong hop le, phai co dinh dang DD/MM/YYYY hh:mm:ss");
  } else {
    try {
      // let hh = Math.floor(ngayChieuGioChieu.slice(11, 13)) + 7;
      // console.log(ngayChieuGioChieu, hh);
      // if (hh > 24) {
      //   hh = hh - 24;
      //   hh = `0${hh}`;
      // }
      // if(hh<7)
      let fixedDate =
        ngayChieuGioChieu.slice(6, 10) +
        "-" +
        ngayChieuGioChieu.slice(3, 5) +
        "-" +
        ngayChieuGioChieu.slice(0, 2) +
        "T" +
        ngayChieuGioChieu.slice(11);
      console.log("ngay", fixedDate.slice(0, 10), fixedDate);

      // const lichDetail = model.findOne({
      //   where: {
      //     // rapId: maRap,
      //     // [Op.like]: `${fixedDate.slice(0, 10)}%`,
      //     [Op.and]: [
      //       { rapId: maRap },
      //       { ngayChieuGioChieu: { [Op.like]: `${fixedDate.slice(0, 10)}%` } },
      //     ],
      //   },
      // });
      const [data] = await sequelize.query(`
      SELECT * FROM database_doanbe.lichchieus
      where rapId=${maRap} and ngayChieuGioChieu like '${fixedDate.slice(
        0,
        10
      )}%';
      `);
      console.log("lichChieu", data);
      if (data.length !== 0) {
        console.log("lich trung", data);
        res.status(500).send("lich chieu da bi trung");
      } else {
        req.ngayChieuGioChieu = fixedDate;
        console.log("next");
        next();
      }
    } catch (err) {
      res.status(500).send(err);
    }
  }
};
module.exports = {
  checkNgayChieuGioChieu,
};
