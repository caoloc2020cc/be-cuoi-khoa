const { Router } = require("express");

const {
  taoLichChieu,
  layDanhSachPhongVe,
  datVe,
} = require("../controller/lichChieu.controller");
const {
  authenticate,
  authorize,
} = require("../middleware/auth/verify-token.middleware");
const {
  checkExist,
} = require("../middleware/validation/checkExist.middleware");
const {
  checkExistMaPhim,
} = require("../middleware/validation/checkExistMaPhim.middleware");
const {
  checkGiaVe,
} = require("../middleware/validation/checkGiaVe.middleware");
const {
  checkMaLichChieu,
} = require("../middleware/validation/checkMaLichChieu.middleware");
const {
  checkMaLichChieuBody,
} = require("../middleware/validation/checkMaLichChieuBody.middleware");
const {
  checkMaRap,
} = require("../middleware/validation/checkMaRap.middleware");
const {
  checkNgayChieuGioChieu,
} = require("../middleware/validation/checkNgayChieuGioChieu.middleware");
const {
  Cineplex,
  Movie,
  Rap,
  sequelize,
  LichChieu,
  Users,
} = require("./../models");
const lichChieuRouter = Router();
lichChieuRouter.post(
  "/",
  authenticate,
  authorize(["QuanTri"]),
  checkExistMaPhim(Movie),
  checkNgayChieuGioChieu(sequelize),
  checkMaRap(Rap),
  checkGiaVe,
  taoLichChieu
);
lichChieuRouter.get("/", checkMaLichChieu(LichChieu), layDanhSachPhongVe);
lichChieuRouter.put(
  "/",
  authenticate,
  authorize(["QuanTri", "KhachHang"]),
  checkMaLichChieuBody(LichChieu),
  checkExist(Users),
  datVe
);
module.exports = {
  lichChieuRouter,
};
