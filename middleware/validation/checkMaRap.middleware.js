const checkMaRap = (model) => async (req, res, next) => {
  const { maRap } = req.body;
  if (!maRap || maRap == true) {
    res.status(400).send("hay nhap ma rap hop le");
  } else {
    console.log("ma rap", maRap);
    try {
      const rapDetail = await model.findOne({
        where: { id: maRap },
      });
      if (rapDetail) {
        next();
      } else {
        res.status(500).send("maRap khong ton tai");
      }
    } catch (err) {
      res.status(500).send(err);
    }
  }
};
module.exports = {
  checkMaRap,
};
