const { userRouter } = require("./user.router");
const express = require("express");
const { authRouter } = require("./auth.router");
const { cinemaRouter } = require("./cinema.router");
const { movieRouter } = require("./movie.router");
const { cineplexRouter } = require("./cineplex");
const { lichChieuRouter } = require("./lichChieu.router");
const rootRouters = express.Router();
rootRouters.use("/users", userRouter);
rootRouters.use("/cinemas", cinemaRouter);
rootRouters.use("/auth", authRouter);
rootRouters.use("/movie", movieRouter);
rootRouters.use("/cineplex", cineplexRouter);
rootRouters.use("/lichChieu", lichChieuRouter);
module.exports = {
  rootRouters,
};
