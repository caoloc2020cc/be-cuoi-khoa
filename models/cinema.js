"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Cinema extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Cineplex, Rap }) {
      // define association here
      this.belongsTo(Cineplex);
      this.hasMany(Rap, { as: "danhSachRap" });
      // this.belongsToMany(Movie, { through: "Rap_Movie" });
    }
  }
  Cinema.init(
    {
      maCumRap: DataTypes.STRING,
      tenCumRap: DataTypes.STRING,
      diaChi: DataTypes.STRING,
      hinhAnh: DataTypes.STRING,
      // danhSachRap: DataTypes.ARRAY(DataTypes.DECIMAL),
      cineplexId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Cinema",
    }
  );
  return Cinema;
};
