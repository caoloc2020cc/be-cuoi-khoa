"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Cineplexes",
      [
        {
          maHeThongRap: "BHDStar",
          tenHeThongRap: "BHD Star Cineplex",
          biDanh: "bhd-star-cineplex",
          logo: "http://movie0706.cybersoft.edu.vn/hinhanh/bhd-star-cineplex.png",
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          maHeThongRap: "CGV",
          tenHeThongRap: "cgv",
          biDanh: "cgv",
          logo: "http://movie0706.cybersoft.edu.vn/hinhanh/cgv.png",
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          maHeThongRap: "CineStar",
          tenHeThongRap: "CineStar",
          biDanh: "cinestar",
          logo: "http://movie0706.cybersoft.edu.vn/hinhanh/cinestar.png",
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          maHeThongRap: "Galaxy",
          tenHeThongRap: "Galaxy Cinema",
          biDanh: "galaxy-cinema",
          logo: "http://movie0706.cybersoft.edu.vn/hinhanh/galaxy-cinema.png",
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          maHeThongRap: "LotteCinima",
          tenHeThongRap: "Lotte Cinema",
          biDanh: "lotte-cinema",
          logo: "http://movie0706.cybersoft.edu.vn/hinhanh/lotte-cinema.png",
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          maHeThongRap: "MegaGS",
          tenHeThongRap: "MegaGS",
          biDanh: "megags",
          logo: "http://movie0706.cybersoft.edu.vn/hinhanh/megags.png",
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
