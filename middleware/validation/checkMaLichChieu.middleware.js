const checkMaLichChieu = (model) => async (req, res, next) => {
  const { maLichChieu } = req.query;

  if (!maLichChieu || maLichChieu === true) {
    res.status(500).send("hay nhap ma lich chieu");
  } else {
    try {
      const lichChieu = await model.findOne({
        where: { id: maLichChieu },
      });
      if (lichChieu) {
        next();
      } else {
        res.status(500).send("maLichChieu khong ton tai");
      }
    } catch (err) {
      res.status(500).send(err);
    }
  }
};
module.exports = {
  checkMaLichChieu,
};
