const { LichChieu, Rap_Movie, Booking, sequelize } = require("./../models");
const taoLichChieu = async (req, res) => {
  const { maPhim, maRap, giaVe } = req.body;
  const { ngayChieuGioChieu } = req;

  try {
    const newRap_Movie = await Rap_Movie.create({
      MovieId: maPhim,
      RapId: maRap,
    });
    const newLichChieu = await LichChieu.create({
      ngayChieuGioChieu,
      rapId: maRap,
    });

    const renderGhe = () => {
      let query = ``;
      for (let i = 1; i <= 160; i++) {
        if (
          (i >= 35 && i <= 46) ||
          (i >= 51 && i <= 62) ||
          (i >= 67 && i <= 78) ||
          (i >= 83 && i <= 94) ||
          (i >= 99 && i <= 110) ||
          (i >= 115 && i <= 126)
        ) {
          query += `('${i}','Vip',${giaVe}+30000,true,null,${newLichChieu.id},'2021-06-22 05:06:02','2021-06-22 05:06:02'),`;
        } else if (i == 160) {
          query += `('${i}','Thuong',${giaVe},false,null,${newLichChieu.id},'2021-06-22 05:06:02','2021-06-22 05:06:02');`;
        } else {
          query += `('${i}','Thuong',${giaVe},false,null,${newLichChieu.id},'2021-06-22 05:06:02','2021-06-22 05:06:02'),`;
        }
      }
      return query;
    };
    // tu dong tao ra 160 ghe
    const newSeat = await sequelize.query(`
        insert into database_doanbe.seats(tenGhe,loaiGhe,giaVe,daDat,taiKhoanNguoiDat,lichChieuId,createdAt,updatedAt) values
          ${renderGhe()}
        `);

    res.status(200).send(newSeat);
  } catch (err) {
    res.status(500).send(err);
  }
};
const layDanhSachPhongVe = async (req, res) => {
  const { maLichChieu } = req.query;
  try {
    const [inFoFilm] = await sequelize.query(`
  SELECT tenPhim,cinemas.diaChi,cinemas.hinhAnh,tenRap,tenCumRap,lichchieus.id as maLichChieu,ngayChieuGioChieu FROM database_doanbe.movies
inner join rap_movies
on movies.id=rap_movies.movieId
inner join raps
on rap_movies.rapId=raps.id
inner join cinemas
on raps.cinemaId=cinemas.id
inner join lichchieus
on raps.id=lichchieus.rapId
where lichchieus.id=${maLichChieu};
  `);
    const [ghe] = await sequelize.query(`
    SELECT seats.id as maGhe,tenGhe,loaiGhe,giaVe,daDat,taiKhoanNguoiDat FROM database_doanbe.lichchieus
inner join seats
on lichchieus.id=seats.lichChieuId
where lichchieus.id=${maLichChieu};
    `);

    inFoFilm[0].danhSachGhe = ghe;
    res.status(200).send(inFoFilm[0]);
  } catch (err) {
    res.status(500).send(err);
  }
};
const datVe = async (req, res) => {
  const { maLichChieu, taiKhoanNguoiDung, danhSachVe } = req.body;
  const { taiKhoanUser } = req;

  let html = ``;
  let date = `${new Date().getFullYear()}-${
    new Date().getMonth() + 1
  }-${new Date().getDate()} ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`;
  danhSachVe.forEach((item, index) => {
    if (index == danhSachVe.length - 1) {
      html += `(${item.maGhe}, true,"${taiKhoanNguoiDung}",'${date}','${date}')`;
    } else {
      html += `(${item.maGhe}, true,"${taiKhoanNguoiDung}",'${date}','${date}'),`;
    }
  });

  try {
    const [update] = await sequelize.query(`
    INSERT INTO database_doanbe.seats(id, daDat,taiKhoanNguoiDat,createdAt,updatedAt)
    VALUES 
    ${html}
    ON DUPLICATE KEY UPDATE 
        taiKhoanNguoiDat = VALUES(taiKhoanNguoiDat),
        daDat=VALUES(daDat),
        createdAt=VALUES(createdAt),
        updatedAt=VALUES(updatedAt);
    `);
    const [inFoFilm] = await sequelize.query(`
    SELECT movies.id FROM database_doanbe.movies
inner join rap_movies
on movies.id=rap_movies.movieId
inner join raps
on rap_movies.rapId=raps.id
inner join lichchieus
on raps.id=lichchieus.rapId
where lichchieus.id=${maLichChieu};
    `);
    // const createBooking = await Booking.create({
    //   userId: taiKhoanUser.id,
    //   movieId: inFoFilm[0].id,
    //   createdAt:
    // });
    const createBooking = await sequelize.query(`
    insert into database_doanbe.bookings(userId,movieId,createdAt,updatedAt) values
    (${taiKhoanUser.id},${inFoFilm[0].id},'${date}','${date}');
    `);
    console.log("infoFlim", inFoFilm[0].id, taiKhoanUser.id);

    res.status(200).send("Dat Ve thanh cong");
  } catch (err) {
    res.status(500).send(err);
  }
};
module.exports = {
  taoLichChieu,
  layDanhSachPhongVe,
  datVe,
};
