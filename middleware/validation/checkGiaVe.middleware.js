const checkGiaVe = (req, res, next) => {
  const { giaVe } = req.body;
  console.log("gia", giaVe);
  if (!giaVe || giaVe == true) {
    res.status(400).send("hay nhap Gia ve");
  } else if (giaVe < 75000 || giaVe > 200000) {
    res.status(500).send("Gia tu 75000 - 200000");
  } else {
    console.log("gia ve");
    next();
  }
};
module.exports = {
  checkGiaVe,
};
