const checkMaHeThongRapQuery = (model) => async (req, res, next) => {
  const { maHeThongRap } = req.query;
  console.log("hello");
  if (maHeThongRap) {
    const heThong = await model.findOne({ where: { maHeThongRap } });
    if (heThong) {
      req.heThongRap = heThong;
      next();
    } else {
      res.status(400).send("Ma He Thong Rap khong ton tai");
    }
  } else {
    res.status(400).send("Ma He Thong Rap khong ton tai");
  }
};
module.exports = {
  checkMaHeThongRapQuery,
};
