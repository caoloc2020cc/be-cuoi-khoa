const { Op } = require("sequelize");
const checkTaiKhoan = (model) => async (req, res, next) => {
  const { id } = req.params;
  const { taiKhoan, email } = req.body;

  if (id) {
    //update
    const userEmail = await model.findAll({
      where: {
        [Op.and]: [{ id: !id }, { email }],
      },
    });
    if (userEmail.length !== 0) {
      res.status(500).send("Email da ton tai");
    } else {
      next();
    }
    // const userTaiKhoan=await model.
  } else {
    // check them email
    const userTaiKhoan = await model.findAll({
      where: {
        [Op.or]: [{ taiKhoan }, { email }],
      },
    });
    if (userTaiKhoan.length !== 0) {
      res.status(500).send("Tai Khoan hoac Email da ton tai");
    } else {
      next();
    }
  }
};

module.exports = {
  checkTaiKhoan,
};
