const { Router } = require("express");
const {
  authenticate,
  authorize,
} = require("../middleware/auth/verify-token.middleware");
const {
  checkMaCumRap,
} = require("../middleware/validation/checkMaCumRap.middleware");
const { Cinema } = require("./../models");
const {
  getAll,
  getCinemaByCineplex,
  uploadHinhAnh,
  createCinema,
  updateCinema,
  deleteCinema,
} = require("./../controller/cinema.controller");
const {
  uploadImageSingle,
} = require("./../middleware/upload/upload-image.middleware");
const cinemaRouter = Router();
cinemaRouter.get("/", getAll);
cinemaRouter.get("/byCineplex", getCinemaByCineplex);
cinemaRouter.post(
  "/upload-hinhAnh/:id",
  uploadImageSingle("hinhAnh"),
  uploadHinhAnh
);
cinemaRouter.post("/", authenticate, authorize(["QuanTri"]), createCinema);
cinemaRouter.put("/", updateCinema);
cinemaRouter.delete(
  "/",
  authenticate,
  authorize(["QuanTri"]),
  checkMaCumRap(Cinema),
  deleteCinema
);
module.exports = {
  cinemaRouter,
};
