"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Movie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Rap, Booking }) {
      // define association here
      this.belongsToMany(Rap, { through: "Rap_Movie" });
      this.hasMany(Booking);
    }
  }
  Movie.init(
    {
      tenPhim: DataTypes.STRING,
      biDanh: DataTypes.STRING,
      trailer: DataTypes.STRING,
      hinhAnh: DataTypes.STRING,
      ngayKhoiChieu: DataTypes.DATE,
      danhGia: DataTypes.INTEGER,
      moTa: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Movie",
    }
  );
  return Movie;
};
