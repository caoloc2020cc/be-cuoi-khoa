const jwt = require("jsonwebtoken");
const authenticate = (req, res, next) => {
  const token = req.header("token");
  try {
    console.log(req);
    const secretKey = "GiangCaoLoc";
    const decode = jwt.verify(token, secretKey);
    req.tokenDecode = decode;

    next();
  } catch (err) {
    res.status(401).send("Ban chua dang nhap");
  }
};
const authorize = (typeArray) => {
  return (req, res, next) => {
    const { tokenDecode } = req;

    const dk =
      typeArray.findIndex((type) => {
        return type == tokenDecode.maLoaiNguoiDung;
      }) > -1;
    if (dk) {
      return next();
    } else {
      res.status(403).send("Ban da dang nhap nhung khong co quyen");
    }
  };
};
module.exports = {
  authenticate,
  authorize,
};
