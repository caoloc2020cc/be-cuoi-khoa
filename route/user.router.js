const express = require("express");
const {
  getListUsers,
  createUsers,
  getDetailUsers,
  removeUsers,
  updateUsers,
  layThongTinTaiKhoan,
} = require("../controller/user.controller");
const {
  authenticate,
  authorize,
} = require("../middleware/auth/verify-token.middleware");
const { logger } = require("../middleware/log/logger.middleware");
const {
  checkEmail,
} = require("../middleware/validation/checkEmail.middleware");
const {
  checkExist,
} = require("../middleware/validation/checkExist.middleware");
const {
  checkTaiKhoan,
} = require("../middleware/validation/checkTaiKhoan.middleware");
// const {
//   checkTaiKhoan,
// } = require("../middleware/validation/checkExistName.middleware");
const { Users } = require("./../models");
const userRouter = express.Router();
userRouter.get("/", logger("lay tat ca nguoi dung"), getListUsers);
userRouter.post(
  "/",
  authenticate,
  authorize(["QuanTri"]),
  // 0 la cac truong hop khac update
  checkEmail(Users, 0),
  checkTaiKhoan(Users),
  createUsers
);
userRouter.get("/layThongTinTaiKhoan", checkExist(Users), layThongTinTaiKhoan);
userRouter.get("/:id", logger("lay chi tiet user"), getDetailUsers);
userRouter.delete(
  "/",
  authenticate,
  authorize(["QuanTri"]),
  checkExist(Users),
  removeUsers
);
userRouter.put(
  "/",
  authenticate,
  authorize(["QuanTri", "KhachHang"]),
  checkExist(Users),
  checkEmail(Users, 1),

  updateUsers
);

// userRouter.post("/")
module.exports = {
  userRouter,
};
