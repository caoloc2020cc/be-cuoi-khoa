"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Booking }) {
      // define association here
      this.hasMany(Booking);
    }
  }
  Users.init(
    {
      hoTen: DataTypes.STRING,
      email: DataTypes.STRING,
      taiKhoan: DataTypes.STRING,
      soDT: DataTypes.STRING,
      maLoaiNguoiDung: DataTypes.STRING,
      matKhau: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Users",
    }
  );
  return Users;
};
