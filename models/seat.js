"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Seat extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ LichChieu }) {
      // define association here
      this.belongsTo(LichChieu);
    }
  }
  Seat.init(
    {
      tenGhe: DataTypes.STRING,
      loaiGhe: DataTypes.STRING,
      giaVe: DataTypes.INTEGER,
      daDat: DataTypes.BOOLEAN,
      taiKhoanNguoiDat: DataTypes.STRING,
      lichChieuId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Seat",
    }
  );
  return Seat;
};
