const checkEmail = (model, number) => async (req, res, next) => {
  const { email } = req.body;

  //number ==1 la update khac 1 la cac truong hop con lai
  if (number == 1) {
    const { taiKhoanUser } = req;
    console.log("tai khoan", taiKhoanUser, taiKhoanUser.email);
    if (email == taiKhoanUser.email) {
      next();
    } else {
      try {
        const userDetail = await model.findOne({ where: { email } });
        console.log(userDetail);
        if (userDetail) {
          res.status(404).send("Email da ton tai");
        } else {
          next();
        }
      } catch (err) {
        res.status(500).send(err);
      }
    }
  } else {
    try {
      const userDetail = await model.findOne({ where: { email } });
      console.log(userDetail);
      if (userDetail) {
        res.status(404).send("Email da ton tai");
      } else {
        next();
      }
    } catch (err) {
      res.status(500).send(err);
    }
  }
};
module.exports = {
  checkEmail,
};
