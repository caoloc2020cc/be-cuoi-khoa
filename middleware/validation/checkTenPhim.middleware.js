const checkTenPhim = (model, number) => async (req, res, next) => {
  const { tenPhim } = req.body;

  if (number == 1) {
    const { movieDetail } = req;
    if (tenPhim == movieDetail.tenPhim) {
      next();
    } else {
      const movieCheck = await model.findOne({ where: { tenPhim } });
      if (movieCheck) {
        res.status(500).send("Ten phim da ton tai");
      } else {
        next();
      }
    }
  } else {
    try {
      const movieDetail = await model.findOne({ where: { tenPhim } });
      if (movieDetail) {
        res.status(404).send("Tên phim đã tồn tại!");
      } else {
        next();
      }
    } catch (err) {
      res.status(500).send(err);
    }
  }
};
module.exports = {
  checkTenPhim,
};
