"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Cinemas",
      [
        {
          tenCumRap: "BHD 3/2",
          diaChi: "150 duong 3/2 quan 10 tphcm",
          hinhAnh: "link hinh BHD 3/2",
          maCumRap: "BHD",
          cineplexId: 1,
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          tenCumRap: "BHD con cho",
          diaChi: "cu chi",
          hinhAnh: "link hinh BHD con cho",
          maCumRap: "BHD",
          cineplexId: 1,
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          tenCumRap: "DDC Nguyen Trai",
          diaChi: "150 duong 3/2 quan 10 tphcm",
          hinhAnh: "link hinh BHD 3/2",
          maCumRap: "DDC",
          cineplexId: 2,
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
