const { Movie, sequelize } = require("./../models");
const getAllMovie = async (req, res) => {
  console.log("getAllMovie");
  const { tenPhim } = req.query;
  // Co query bang tenPhim hoac khong co
  if (tenPhim) {
    try {
      const [data] = await sequelize.query(`
      SELECT id as maPhim, tenPhim,biDanh,trailer,hinhAnh,ngayKhoiChieu,danhGia,moTa FROM database_doanbe.movies
      where movies.tenPhim LIKE "%${tenPhim}%";
        `);
      res.status(200).send(data);
    } catch (err) {
      res.status(500).send(err);
    }
  } else {
    try {
      const movieList = await Movie.findAll({
        attributes: [
          ["id", "maPhim"],
          "tenPhim",
          "biDanh",
          "trailer",
          "hinhAnh",
          "ngayKhoiChieu",
          "moTa",
          "danhGia",
        ],
      });
      res.status(200).send(movieList);
    } catch (err) {
      res.status(500).send(err);
    }
  }
};
const getDetailMovie = async (req, res) => {
  const { maPhim } = req.query;

  try {
    const movieDetail = await Movie.findByPk(maPhim);
    if (movieDetail) {
      res.status(200).send(movieDetail);
    } else {
      res.status(404).send("Not Found");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
const createMovie = async (req, res) => {
  const { tenPhim, biDanh, trailer, hinhAnh, moTa, danhGia } = req.body;
  const { ngayKhoiChieu } = req;

  try {
    const newMovie = await Movie.create({
      tenPhim,
      biDanh,
      trailer,
      hinhAnh,
      ngayKhoiChieu,
      moTa,
      danhGia,
    });
    res.status(201).send(newMovie);
  } catch (err) {
    res.status(500).send(err);
  }
};
const updatePhim = async (req, res) => {
  const {
    maPhim,
    tenPhim,
    biDanh,
    trailer,
    hinhAnh,
    moTa,

    danhGia,
  } = req.body;
  const { ngayKhoiChieu } = req;
  try {
    const [result] = await Movie.update(
      { tenPhim, biDanh, trailer, hinhAnh, moTa, ngayKhoiChieu, danhGia },
      { where: { id: maPhim } }
    );
    if (result > 0) {
      res.status(200).send("Update thanh cong");
    } else {
      res.status(404).send("Not Found");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
const deleteMovie = async (req, res) => {
  const { maPhim } = req.query;
  try {
    await Movie.destroy({
      where: { id: maPhim },
    });
    res.status(200).send("Xoa Thanh Cong");
  } catch (err) {
    res.status(500).send(err);
  }
};
const uploadHinhAnhMovie = async (req, res) => {
  const { file } = req;
  const { maPhim } = req.query;
  const port = process.env.PORT || 3000;
  const urlHinhAnh = `http://localhost:${port}/${file.path}`;
  try {
    const MovieDetail = await Movie.findByPk(maPhim);
    MovieDetail.hinhAnh = urlHinhAnh;
    await MovieDetail.save();
    res.status(200).send(MovieDetail);
  } catch (err) {
    res.status(500).send(err);
  }
};
const layDanhSachPhimTheoNgay = async (req, res) => {
  const { tuNgay, denNgay, tenPhim } = req.query;

  try {
    const [listMovie] = await sequelize.query(`
      SELECT * FROM database_doanbe.movies
  ${tuNgay || denNgay || tenPhim ? `where` : ""} ${
      tuNgay ? `movies.ngayKhoiChieu>='${tuNgay}'` : ""
    } ${tuNgay && denNgay ? "and" : ""} ${
      denNgay ? `movies.ngayKhoiChieu<='${denNgay}'` : ""
    } ${(tuNgay || denNgay) && tenPhim ? "and" : ""} ${
      tenPhim ? `tenPhim like '%${tenPhim}%'` : ""
    };
      `);
    res.status(200).send(listMovie);
  } catch (err) {
    res.status(500).send(err);
  }
};
module.exports = {
  getAllMovie,
  createMovie,
  updatePhim,
  deleteMovie,
  getDetailMovie,
  uploadHinhAnhMovie,
  layDanhSachPhimTheoNgay,
};
