const logger = (mes) => (req, res, next) => {
  console.log(mes);
  next();
};
module.exports = {
  logger,
};
