const checkExistMaPhimQuery = (model) => async (req, res, next) => {
  // co 2 loai check Tai khoan exist la de xem co ton tai 2 check tai khoan de xem co bi trung
  const { maPhim } = req.query;
  if (maPhim) {
    const movieDetail = await model.findOne({ where: { id: maPhim } });
    if (movieDetail) {
      //truyen movieDetail xuong duoi ai nhan thi nhan
      req.movieDetail = movieDetail;
      console.log("qua");
      next();
    } else {
      console.log("truot");
      res.status(404).send("Ma phim khong ton tai");
    }
  } else {
    res.status(404).send("Ma phim khong ton tai");
  }
};
module.exports = {
  checkExistMaPhimQuery,
};
