const checkMaCumRap = (model) => async (req, res, next) => {
  const { maCumRap } = req.body;
  try {
    const cinemaDetail = await model.findOne({
      where: { maCumRap },
    });
    if (cinemaDetail) {
      next();
    } else {
      res.status(500).send("MaCumRap khong ton tai");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
module.exports = {
  checkMaCumRap,
};
