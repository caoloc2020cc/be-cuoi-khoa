"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Cinemas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      maCumRap: {
        type: Sequelize.STRING,
      },
      tenCumRap: {
        type: Sequelize.STRING,
      },
      diaChi: {
        type: Sequelize.STRING,
      },
      hinhAnh: {
        type: Sequelize.STRING,
      },
      // danhSachRap: {
      //   type: Sequelize.ARRAY(ABSTRACT),
      // },
      cineplexId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Cineplexes",
          key: "id",
        },
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Cinemas");
  },
};
