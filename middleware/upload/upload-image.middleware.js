const multer = require("multer");
const mkdirp = require("mkdirp");
const uploadImageSingle = (type) => {
  mkdirp.sync(`./public/images/${type}`);
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, `./public/images/${type}`);
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + "_" + file.originalname);
    },
  });
  const upload = multer({
    storage,
  });
  return upload.single(type);
};
module.exports = {
  uploadImageSingle,
};
