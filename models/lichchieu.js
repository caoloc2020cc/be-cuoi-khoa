"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class LichChieu extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Rap, Seat }) {
      // define association here
      this.belongsTo(Rap);
      this.hasMany(Seat);
    }
  }
  LichChieu.init(
    {
      ngayChieuGioChieu: DataTypes.STRING,
      rapId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "LichChieu",
    }
  );
  return LichChieu;
};
