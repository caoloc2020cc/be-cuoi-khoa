const {
  Cinema,
  Cineplex,
  Movie,
  LichChieu,
  Rap,
  sequelize,
} = require("./../models");
const { Op } = require("sequelize");
const getAllCineplex = async (req, res) => {
  const { maHeThongRap } = req.query;
  if (maHeThongRap) {
    try {
      const [data] = await sequelize.query(`
        SELECT maHeThongRap,tenHeThongRap,biDanh,logo FROM database_doanbe.cineplexes
        where maHeThongRap="${maHeThongRap}";
        `);
      res.status(200).send(data);
    } catch (err) {
      res.status(500).send(err);
    }
  } else {
    try {
      const cineplexList = await Cineplex.findAll({
        attributes: ["maHeThongRap", "tenHeThongRap", "biDanh", "logo"],
      });
      res.status(200).send(cineplexList);
    } catch (err) {
      res.status(500).send(err);
    }
  }
};
const getAllCinemaByCineplex = async (req, res) => {
  const { heThongRap } = req;
  console.log("helllo", heThongRap.id);
  try {
    const cinemaList = await Cinema.findAll({
      //   where: { cineplexId: maHeThongRap },
      where: { cineplexId: heThongRap.id },
      attributes: ["tenCumRap", "diaChi", "hinhAnh", "maCumRap"],
      include: { model: Rap, as: "danhSachRap" },
    });
    // const cinemaList = await Cinema.findAll();
    res.status(200).send(cinemaList);
  } catch (err) {
    res.status(500).send(err);
  }
};
const getLichChieuHeThongRap = async (req, res) => {
  const { maHeThongRap } = req.query;
  console.log("helo");
  if (maHeThongRap) {
  } else {
    try {
      const lichChieuList = await Cineplex.findAll({
        include: {
          model: Cinema,
          include: {
            model: Rap,
            as: "danhSachRap",
            include: [Movie, LichChieu],
          },
        },
      });

      res.status(200).send(lichChieuList);
    } catch (err) {
      res.status(500).send(err);
    }
  }
};
const getLichChieuPhim = async (req, res) => {
  // api lay thong tin lich chieu theo  ma phim
  console.log("get lich chieu");
  const { movieDetail } = req;

  try {
    console.log("get");

    const [data] = await sequelize.query(`
    SELECT lichchieus.id as maLichChieu,raps.id as maRap,tenRap,ngayChieuGioChieu, cinemaId from database_doanbe.raps
    inner join lichchieus
    on raps.id=lichchieus.rapId
    inner join rap_movies
    on raps.id=rap_movies.rapId
    inner join movies
    on rap_movies.movieId=movies.id
    where movies.id=${movieDetail.id};
    `);

    let lichChieuList = await Cineplex.findAll({
      include: Cinema,
    });

    lichChieu = lichChieuList.map((item) => {
      if (item.Cinemas.length !== 0) {
        itemKhong = item.Cinemas.map((rap) => {
          let arr = [];
          data.forEach((item) => {
            if (rap.id == item.cinemaId) {
              arr.push(item);
            }
          });
          rap.dataValues.danhSachRap = arr;
          return rap;
        });
        item.Cinemas = itemKhong;
        return item;
      }
    });
    lichChieu = lichChieu.filter((item) => {
      if (item == null) {
        return false;
      }
      itemKhong = item.Cinemas.filter((rap) => {
        return rap.dataValues.danhSachRap.length !== 0;
      });

      item.dataValues.Cinemas = itemKhong;

      return item.dataValues.Cinemas.length !== 0;
    });

    movieDetail.dataValues.listHeThongRap = [...lichChieu];

    res.status(200).send(movieDetail);
  } catch (err) {
    res.status(500).send(err);
  }
};
const createCineplex = async (req, res) => {
  const { maHeThongRap, tenHeThongRap, biDanh, logo } = req.body;
  try {
    const newCineplex = await Cineplex.create({
      maHeThongRap,
      tenHeThongRap,
      biDanh,
      logo,
    });
    res.status(201).send(newCineplex);
  } catch (err) {
    res.status(500).send(err);
  }
};
const updateCineplex = async (req, res) => {
  const { tenHeThongRap, biDanh, logo } = req.body;
  const { maHeThongRap } = req.query;
  console.log("truyen len", tenHeThongRap, biDanh, logo, maHeThongRap);
  try {
    const [result] = await Cineplex.update(
      {
        tenHeThongRap,
        biDanh,
        logo,
      },
      { where: { maHeThongRap } }
    );
    if (result > 0) {
      res.status(200).send("Update thanh cong");
    } else {
      res.status(404).send("Not Found");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
const removeCineplex = async (req, res) => {
  const { maHeThongRap } = req.query;
  try {
    await Cineplex.destroy({
      where: {
        maHeThongRap,
      },
    });
    res.status(200).send("Xoa thanh cong");
  } catch (err) {
    res.status(500).send(err);
  }
};
module.exports = {
  getAllCineplex,
  getAllCinemaByCineplex,
  getLichChieuHeThongRap,
  getLichChieuPhim,
  createCineplex,
  updateCineplex,
  removeCineplex,
};
