const { buildSchema } = require("graphql");
const graphqlSchema = buildSchema(`
type Users{
    id:Int!
hoTen:String!
email:String!
taiKhoan:String!
soDT:String!
maLoaiNguoiDung:String!
matKhau:String!
}
type rootQuery{
    user(id:Int):Users!
    users:[Users]!
}
input Inputuser{
    hoTen:String!
    email:String!
    taiKhoan:String!
    soDT:String!
    maLoaiNguoiDung:String!
    matKhau:String!
}
type rootMutation{
    createUser(inputUser:Inputuser):Users!
    removeUser(id:Int):Users!
}
schema{
    query:rootQuery
    mutation:rootMutation
}
`);
module.exports = {
  graphqlSchema,
};
