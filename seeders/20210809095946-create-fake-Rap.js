"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    //  */
    await queryInterface.bulkInsert(
      "Raps",
      [
        {
          id: 701,
          tenRap: "Rạp 1",
          cinemaId: 1,
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          id: 702,
          tenRap: "Rạp 2",
          cinemaId: 1,
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          id: 703,
          tenRap: "Rạp 3",
          //giaVe
          cinemaId: 1,
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          id: 704,
          tenRap: "Rạp 4",
          cinemaId: 1,
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          id: 705,
          tenRap: "Rạp 5",
          cinemaId: 1,
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          id: 706,
          tenRap: "Rạp 6",
          cinemaId: 1,
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          id: 707,
          tenRap: "Rạp 7",
          cinemaId: 1,
          createdAt: "2021-07-22 05:06:02",
          updatedAt: "2021-07-22 05:06:02",
        },
        {
          id: 601,
          tenRap: "Rạp 1",
          cinemaId: 2,
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          id: 602,
          tenRap: "Rạp 2",
          cinemaId: 2,
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          id: 603,
          tenRap: "Rạp 3",
          cinemaId: 2,
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          id: 504,
          tenRap: "Rạp 4",
          cinemaId: 3,
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          id: 501,
          tenRap: "Rạp 1",
          cinemaId: 3,
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          id: 502,
          tenRap: "Rạp 2",
          cinemaId: 3,
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
