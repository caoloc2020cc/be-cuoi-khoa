const { Users } = require("./../models");
const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");
const signIn = async (req, res) => {
  const { taiKhoan, matKhau } = req.body;

  try {
    const userLogin = await Users.findOne({ where: { taiKhoan } });

    if (userLogin) {
      const isAuth = bcryptjs.compareSync(matKhau, userLogin.matKhau);
      console.log(isAuth);
      if (isAuth) {
        const payload = {
          id: userLogin.id,
          taiKhoan: userLogin.taiKhoan,
          maLoaiNguoiDung: userLogin.maLoaiNguoiDung,
        };
        const secretKey = "GiangCaoLoc";
        const token = jwt.sign(payload, secretKey, {
          expiresIn: 12 * 30 * 24 * 60 * 60,
        });
        res.status(200).send({ message: "Dang nhap thanh cong", token });
      } else {
        res.status(400).send("Mat khau khong chinh xac");
      }
    } else {
      res.status(404).send("Tài Khoản không tồn tại");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
const signUp = async (req, res) => {
  const { hoTen, email, taiKhoan, soDT, matKhau } = req.body;
  try {
    const salt = bcryptjs.genSaltSync(10);
    const hashPassword = bcryptjs.hashSync(matKhau, salt);

    const newUser = await Users.create({
      hoTen,
      email,
      taiKhoan,
      soDT,
      matKhau: hashPassword,
      maLoaiNguoiDung: "KhachHang",
    });
    res.status(201).send(newUser);
  } catch (err) {
    res.status(500).send(err);
  }
};
module.exports = {
  signIn,
  signUp,
};
