const { createUsers, removeUsers } = require("../controller/user.controller");
const { Users } = require("./../models");
const graphqlResolvers = {
  async user({ id }) {
    console.log(id);
    try {
      const user = await Users.findByPk(id);
      return user;
    } catch (err) {
      throw new Error(err);
    }
    // return {
    //   id: 1,
    //   hoTen: "Loc",
    //   email: "dfgj@gmail.com",
    //   taiKhoan: "caoLoc",
    //   soDT: "0987473",
    //   maLoaiNguoiDung: "KhachHang",
    //   matKhau: "1234",
    // };
  },
  async users() {
    try {
      const users = await Users.findAll();
      return users;
    } catch (err) {
      throw new Error(err);
    }
  },
  async createUser({ inputUser }) {
    console.log(inputUser);
    try {
      const user = Users.create({ ...inputUser });
      return user;
    } catch (err) {
      throw new Error(err);
    }
  },
  async removeUser({ id }) {
    try {
      const user = await Users.findByPk(id);
      if (user) {
        await Users.destroy({
          where: {
            id,
          },
        });
        return user;
      } else {
        throw new Error("Not Found");
      }
    } catch (err) {
      throw new Error(err);
    }
  },
};

module.exports = {
  graphqlResolvers,
};
