const cinema = require("../models/cinema");
const { Cinema, Cineplex, sequelize } = require("./../models");
const getAll = async (req, res) => {
  console.log("get all");
  try {
    const cinemaList = await Cinema.findAll({
      include: [
        {
          model: Cineplex,
        },
      ],
    });
    res.status(200).send(cinemaList);
  } catch (err) {
    res.status(500).send(err);
  }
};

const getCinemaByCineplex = async (req, res) => {
  const { id, maCumRap } = req.query;
  console.log("getBy");
  try {
    const [data] = await sequelize.query(`
   
    select cinemas.tenCumRap,cinemas.diaChi,cinemas.hinhAnh from cinemas
    inner join cineplexes
    on cinemas.cineplexId=cineplexes.id
    where cinemas.maCumRap="${maCumRap}";
        `);
    res.status(200).send(data);
  } catch (err) {
    res.status(500).send(err);
  }
};
const createCinema = async (req, res) => {
  const { maCumRap, tenCumRap, diaChi, hinhAnh, cineplexId } = req.body;
  const checkMaCumRap = await Cinema.findOne({
    where: { maCumRap },
  });
  if (checkMaCumRap) {
    res.status(500).send("maCumRap da ton tai");
  } else {
    try {
      const newCinema = await Cinema.create({
        maCumRap,
        tenCumRap,
        diaChi,
        hinhAnh,
        cineplexId,
      });
      console.log("cinema", newCinema.id);
      // tu dong tao ra 5 rap
      const renderRap = () => {
        let query = ``;
        for (let i = 1; i <= 5; i++) {
          if (i == 5) {
            query += `('Rạp ${i}',${newCinema.id},'2021-06-22 05:06:02','2021-06-22 05:06:02');`;
          } else {
            query += `('Rạp ${i}',${newCinema.id},'2021-06-22 05:06:02','2021-06-22 05:06:02'),`;
          }
        }
        return query;
      };
      const createRap = await sequelize.query(`
      insert into database_doanbe.raps(tenRap,cinemaId,createdAt,updatedAt) values
      ${renderRap()}
      `);
      res.status(201).send(newCinema);
    } catch (err) {
      res.status(500).send(err);
    }
  }
};
const uploadHinhAnh = async (req, res) => {
  const { file } = req;
  const { id } = req.params;
  const port = process.env.PORT || 3000;
  const urlHinhAnh = `http://localhost:${port}/${file.path}`;
  try {
    const cinemaDetail = await Cinema.findByPk(id);
    cinemaDetail.hinhAnh = urlHinhAnh;
    await cinemaDetail.save();
    res.status(200).send(cinemaDetail);
  } catch (err) {
    res.status(500).send(err);
  }
};
const updateCinema = async (req, res) => {
  const { id, maCumRap, tenCumRap, diaChi, hinhAnh, cineplexId } = req.body;
  try {
    const [result] = await Cinema.update(
      {
        maCumRap,
        tenCumRap,
        diaChi,
        hinhAnh,
        cineplexId,
      },
      {
        where: { id },
      }
    );
    if (result > 0) {
      res.status(200).send("Update thanh cong");
    } else {
      res.status(404).send("NotFound");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
const deleteCinema = async (req, res) => {
  const { maCumRap } = req.body;
  try {
    await Cinema.destroy({
      where: {
        maCumRap,
      },
    });
    res.status(200).send("Xoa thanh cong");
  } catch (err) {
    res.status(500).send(err);
  }
};
module.exports = {
  getAll,
  getCinemaByCineplex,
  uploadHinhAnh,
  createCinema,
  updateCinema,
  deleteCinema,
};
