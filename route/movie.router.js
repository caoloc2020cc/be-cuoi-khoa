const { Router } = require("express");
const {
  getAllMovie,
  createMovie,
  updatePhim,
  deleteMovie,
  getDetailMovie,
  uploadHinhAnhMovie,
  layDanhSachPhimTheoNgay,
} = require("../controller/movie.controller");
const {
  authenticate,
  authorize,
} = require("../middleware/auth/verify-token.middleware");
const {
  uploadImageSingle,
} = require("../middleware/upload/upload-image.middleware");
const {
  checkExistMaPhim,
} = require("../middleware/validation/checkExistMaPhim.middleware");
const {
  checkExistMaPhimQuery,
} = require("../middleware/validation/checkExistMaPhimQuery.middleware");
const {
  checkNgayKhoiChieu,
} = require("../middleware/validation/checkNgayKhoiChieu.middleware");
const {
  checkTenPhim,
} = require("../middleware/validation/checkTenPhim.middleware");
const {
  checkTuNgayDenNgay,
} = require("../middleware/validation/checkTuNgayDenNgay.middleware");
const { Movie } = require("./../models");

const movieRouter = Router();
movieRouter.get(
  "/getDetailMovie",
  checkExistMaPhimQuery(Movie),
  getDetailMovie
);
movieRouter.get(
  "/layDanhSachPhimTheoNgay",
  checkTuNgayDenNgay,
  layDanhSachPhimTheoNgay
);
movieRouter.get("/", getAllMovie);

movieRouter.post(
  "/",
  authenticate,
  authorize(["QuanTri"]),
  checkTenPhim(Movie, 0),
  checkNgayKhoiChieu,
  createMovie
);
movieRouter.put(
  "/",
  authenticate,
  authorize(["QuanTri"]),
  checkExistMaPhim(Movie),
  checkTenPhim(Movie, 1),
  checkNgayKhoiChieu,
  updatePhim
);
movieRouter.delete("/", checkExistMaPhimQuery(Movie), deleteMovie);
movieRouter.post(
  "/upload-hinhAnh",
  checkExistMaPhimQuery(Movie),
  uploadImageSingle("hinhAnh"),
  uploadHinhAnhMovie
);
module.exports = {
  movieRouter,
};
