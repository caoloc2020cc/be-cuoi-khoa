"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "LichChieus",
      [
        {
          ngayChieuGioChieu: "2021-05-22 05:06:02",
          rapId: "701",
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          ngayChieuGioChieu: "2021-06-22 05:00:00",
          rapId: "705",
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          ngayChieuGioChieu: "2021-06-22 05:00:00",
          rapId: "601",
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          ngayChieuGioChieu: "2021-06-22 05:00:00",
          rapId: "603",
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          ngayChieuGioChieu: "2021-06-22 05:00:00",
          rapId: "501",
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
