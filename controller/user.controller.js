const { Users, sequelize } = require("./../models");
const bcryptjs = require("bcryptjs");
// ds User
const getListUsers = async (req, res) => {
  try {
    const userList = await Users.findAll();
    res.status(200).send(userList);
  } catch (err) {
    res.status(500).send(err);
  }
};
//lay chi tiet
const getDetailUsers = async (req, res) => {
  const { id } = req.params;
  try {
    const userDetail = await Users.findByPk(id);

    if (userDetail) {
      res.status(200).send(userDetail);
    } else {
      res.status(404).send("Not Found");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
// tao User
const createUsers = async (req, res) => {
  const { hoTen, email, taiKhoan, soDT, maLoaiNguoiDung, matKhau } = req.body;
  console.log(req.body);
  try {
    const salt = bcryptjs.genSaltSync(10);
    const hashPassword = bcryptjs.hashSync(matKhau, salt);
    const newUser = await Users.create({
      hoTen,
      email,
      taiKhoan,
      soDT,
      maLoaiNguoiDung,
      matKhau: hashPassword,
    });
    res.status(201).send(newUser);
  } catch (err) {
    res.status(500).send(err);
  }
};
const removeUsers = async (req, res) => {
  const { taiKhoan } = req.body;
  try {
    await Users.destroy({
      where: {
        taiKhoan,
      },
    });
    res.status(200).send("Xoa Thanh Cong");
  } catch (err) {
    res.status(500).send(err);
  }
};
// update user
const updateUsers = async (req, res) => {
  const { hoTen, email, taiKhoan, soDT, maLoaiNguoiDung } = req.body;
  try {
    const [result] = await Users.update(
      { hoTen, email, taiKhoan, soDT, maLoaiNguoiDung },
      { where: { taiKhoan } }
    );

    if (result > 0) {
      res.status(200).send("Update thanh cong");
    } else {
      res.status(404).send("Not Found");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
const layThongTinTaiKhoan = async (req, res) => {
  const { taiKhoanUser } = req;
  const { taiKhoan } = req.body;
  try {
    const [list] = await sequelize.query(`
    select bookings.id as maVe,bookings.createdAt as ngayDat,tenPhim from users
    inner join bookings
    on users.id=bookings.userId
    inner join movies
    on bookings.movieId=movies.id
    where users.taiKhoan=${taiKhoan};
    
    `);
    const [seat] = await sequelize.query(`
    select maHeThongRap,tenHeThongRap,maCumRap,tenCumRap,raps.id as maRap,tenRap,loaiGhe,giaVe,seats.id as maGhe,tenGhe,seats.createdAt from cineplexes
    inner join cinemas
    on cineplexes.id=cinemas.cineplexId
    inner join raps
    on cinemas.id=raps.cinemaId
    inner join lichchieus
    on raps.id=lichchieus.rapId
    inner join seats
    on lichchieus.id=seats.lichChieuId
    where seats.taiKhoanNguoiDat=${taiKhoan};
    `);
    console.log(list);
    list.forEach((item) => {
      let danhSachGhe = [];
      seat.forEach((ghe) => {
        if (item.ngayDat.toString() == ghe.createdAt.toString()) {
          danhSachGhe.push(ghe);
        }
      });
      // console.log("item",item)
      item.danhSachGhe = danhSachGhe;
    });
    taiKhoanUser.dataValues.thongTinDatVe = list;

    res.status(200).send(taiKhoanUser);
  } catch (err) {
    res.status(500).send(err);
  }
};
module.exports = {
  getListUsers,
  createUsers,
  getDetailUsers,
  removeUsers,
  updateUsers,
  layThongTinTaiKhoan,
};
