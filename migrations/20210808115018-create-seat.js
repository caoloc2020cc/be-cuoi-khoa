"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Seats", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      tenGhe: {
        type: Sequelize.STRING,
      },
      loaiGhe: {
        type: Sequelize.STRING,
      },
      giaVe: {
        type: Sequelize.INTEGER,
      },
      daDat: {
        type: Sequelize.BOOLEAN,
      },
      taiKhoanNguoiDat: {
        type: Sequelize.STRING,
      },
      lichChieuId: {
        type: Sequelize.INTEGER,
        references: {
          model: "LichChieus",
          key: "id",
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Seats");
  },
};
