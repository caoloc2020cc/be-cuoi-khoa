"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Rap_Movies",
      [
        {
          rapId: "601",
          movieId: "3",
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          rapId: "603",
          movieId: "3",
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          rapId: "705",
          movieId: "2",
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          rapId: "501",
          movieId: "1",
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
        {
          rapId: "502",
          movieId: "1",
          createdAt: "2021-06-22 05:06:02",
          updatedAt: "2021-06-22 05:06:02",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
