const checkTuNgayDenNgay = async (req, res, next) => {
  const { tuNgay, denNgay } = req.query;
  if (
    tuNgay &&
    !tuNgay.match(
      /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/
    )
  ) {
    res
      .status(500)
      .send(
        "Ngày chiếu không hợp lệ, Ngày chiếu phải có định dạng yyyy-MM-dd !"
      );
  } else if (
    denNgay &&
    !denNgay.match(
      /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/
    )
  ) {
    res
      .status(500)
      .send(
        "Ngày chiếu không hợp lệ, Ngày chiếu phải có định dạng yyyy-MM-dd !"
      );
  } else {
    next();
  }
};
module.exports = {
  checkTuNgayDenNgay,
};
