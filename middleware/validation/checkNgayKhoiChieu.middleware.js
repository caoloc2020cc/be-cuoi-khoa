const checkNgayKhoiChieu = (req, res, next) => {
  const { ngayKhoiChieu } = req.body;
  let fixedDate = "";
  if (ngayKhoiChieu == "" || ngayKhoiChieu == true || !ngayKhoiChieu) {
    res
      .status(500)
      .send(
        "Ngày chiếu không hợp lệ, Ngày chiếu phải có định dạng dd-MM-yyyy !"
      );
  }
  if (
    ngayKhoiChieu &&
    !ngayKhoiChieu.match(
      /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/
    )
  ) {
    res
      .status(500)
      .send(
        "Ngày chiếu không hợp lệ, Ngày chiếu phải có định dạng dd-MM-yyyy !"
      );
  } else {
    fixedDate =
      ngayKhoiChieu.slice(6, 10) +
      "-" +
      ngayKhoiChieu.slice(3, 5) +
      "-" +
      ngayKhoiChieu.slice(0, 2);
    console.log(
      "ngay",
      fixedDate,
      ngayKhoiChieu.slice(6, 10),
      ngayKhoiChieu.slice(3, 5),
      ngayKhoiChieu.slice(0, 2)
    );
    req.ngayKhoiChieu = fixedDate;
    next();
  }
};
module.exports = {
  checkNgayKhoiChieu,
};
