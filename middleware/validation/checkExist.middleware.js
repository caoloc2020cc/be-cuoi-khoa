const checkExist = (model) => async (req, res, next) => {
  // co 2 loai check Tai khoan exist la de xem co ton tai 2 check tai khoan de xem co bi trung
  const { taiKhoan, taiKhoanNguoiDung } = req.body;
  let a = taiKhoanNguoiDung || taiKhoan;
  console.log(taiKhoan);
  if (!a || a === true) {
    res.status(500).send("Nhap tai khoan di");
  } else {
    try {
      const userDetail = await model.findOne({ where: { taiKhoan: a } });
      if (userDetail) {
        //truyen userDetail xuong duoi ai nhan thi nhan
        req.taiKhoanUser = userDetail;
        next();
      } else {
        res.status(404).send("Tai khoan khong ton tai");
      }
    } catch (err) {
      res.status(500).send(err);
    }
  }
};
module.exports = {
  checkExist,
};
