"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Rap extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ Cinema, LichChieu, Movie }) {
      // define association here
      this.belongsTo(Cinema);
      this.hasMany(LichChieu);
      this.belongsToMany(Movie, { through: "Rap_Movie" });
    }
  }
  Rap.init(
    {
      tenRap: DataTypes.STRING,
      cinemaId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Rap",
    }
  );
  return Rap;
};
