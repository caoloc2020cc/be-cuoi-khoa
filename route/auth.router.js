const express = require("express");
const { Users } = require("./../models");
const { signIn, signUp } = require("../controller/auth.controller");
const {
  checkTaiKhoan,
} = require("../middleware/validation/checkTaiKhoan.middleware");
const {
  checkEmail,
} = require("../middleware/validation/checkEmail.middleware");
const authRouter = express.Router();
authRouter.post("/sign-in", signIn);
authRouter.post("/sign-up", checkTaiKhoan(Users), checkEmail(Users), signUp);
module.exports = {
  authRouter,
};
