const express = require("express");
const { rootRouters } = require("./route/root.routers");
const app = express();
const path = require("path");
const { graphqlHTTP } = require("express-graphql");
const { graphqlSchema } = require("./graphql/schema");
const { graphqlResolvers } = require("./graphql/resolvers");
const cors = require("cors");
app.use(cors());
app.use(
  "/graphql",
  graphqlHTTP({
    schema: graphqlSchema,
    rootValue: graphqlResolvers,
    graphiql: true,
  })
);
app.get("/", (req, res) => {
  res.send("hello cyber soft");
});

//set up express ve json
app.use(express.json());
app.use("/api/v1", rootRouters);

const publicPathDirectory = path.join(__dirname, "./public");
app.use("/public", express.static(publicPathDirectory));
//set port
const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`app run on ${port}`);
});
