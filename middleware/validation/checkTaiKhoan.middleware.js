const checkTaiKhoan = (model) => async (req, res, next) => {
  const { taiKhoan } = req.body;
  try {
    console.log(taiKhoan);
    const userDetail = await model.findOne({ where: { taiKhoan } });

    if (userDetail) {
      res.status(404).send("Tai khoan da ton tai");
    } else {
      next();
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
module.exports = {
  checkTaiKhoan,
};
