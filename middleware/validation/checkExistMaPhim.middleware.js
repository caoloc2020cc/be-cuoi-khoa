const checkExistMaPhim = (model) => async (req, res, next) => {
  // co 2 loai check Tai khoan exist la de xem co ton tai 2 check tai khoan de xem co bi trung
  const { maPhim } = req.body;
  console.log(maPhim, req.body);
  if (!maPhim) {
    res.status(400).send("Ban hay nhap ma phim");
  }
  try {
    const movieDetail = await model.findOne({ where: { id: maPhim } });
    if (movieDetail) {
      //truyen movieDetail xuong duoi ai nhan thi nhan
      req.movieDetail = movieDetail;
      console.log("qua");
      next();
    } else {
      console.log("truot");
      res.status(404).send("Ma phim khong ton tai");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
module.exports = {
  checkExistMaPhim,
};
